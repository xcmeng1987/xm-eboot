package com.xm.eboot.generator;

import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.jupiter.api.Test;

public class JasyptTest {
	@Test
	public void contextLoads() {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword("xiaomeng");
		String username = textEncryptor.encrypt("m_boot");
		String password = textEncryptor.encrypt("m_boot");
		String redisPass = textEncryptor.encrypt("123456");
		System.out.println("username:" + username);
		System.out.println("password:" + password);
		System.out.println("redisPass:" + redisPass);
	}
}
