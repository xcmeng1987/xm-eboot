package com.xm.eboot.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xm.eboot.common.config.redis.limit.Limit;
import com.xm.eboot.common.config.redis.service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 测试前端控制器
 * </p>
 *
 * @author mengxc
 * @since 2021-07-01
 */
@RestController
@RequestMapping("/sys-info")
@Api(value = "测试前端控制器", tags = "测试前端控制器")
public class TestController {

	@Autowired
	private RedisService redisService;

	/**
	 * @Title: test1
	 * @Description: 分布式锁测试(1)
	 * @param request
	 * @param username
	 * @return
	 */
	@ApiOperation("测试1")
	@GetMapping("/test1")
	public String test1(HttpServletRequest request, String username) {
		String u = UUID.randomUUID().toString();
		try {
			boolean bool = redisService.tryLock(username, u, 120, 5000);
			System.out.println(bool ? "获取到锁" : "未获取到锁");
			Thread.sleep(4000L);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			redisService.releaseLock(username, u);
		}
		return "ok";
	}

	/**
	 * @Title: test3
	 * @Description: 分布式锁测试(2)
	 * @param request
	 * @param username
	 * @return
	 */
	@ApiOperation("测试3")
	@GetMapping("/test3")
	public String test3(HttpServletRequest request, String username) {
		String u = UUID.randomUUID().toString();
		try {
			boolean bool = redisService.tryLock(username, u, 120, 5000);
			System.out.println(bool ? "获取到锁" : "未获取到锁");
			Thread.sleep(4000L);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			redisService.releaseLock(username, u);
		}
		return "ok";
	}

	/**
	 * @Title: test2
	 * @Description: 限流测试
	 * @param request
	 * @param username
	 * @return
	 */
	@ApiOperation("测试2")
	@GetMapping("/test2")
	@Limit(name = "测试2", key = "test2", count = 2, period = 5)
	public String test2(HttpServletRequest request, String username) {
		return "ok";
	}

}
