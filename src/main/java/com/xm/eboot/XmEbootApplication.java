package com.xm.eboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XmEbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(XmEbootApplication.class, args);
	}

}
