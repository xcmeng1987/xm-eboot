package com.xm.eboot.common.config.redis.service;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: RedisService
 * @Description: spring redis 工具类
 * @author mengxc
 * @date 2020年9月9日 下午3:25:11
 */
@Slf4j
@Component
@SuppressWarnings(value = { "unchecked", "rawtypes" })
public class RedisService {

	private static final Long SUCCESS = 1L;
	
	@Autowired
	public RedisTemplate redisTemplate;

	/**
	 * 缓存基本的对象，Integer、String、实体类等
	 *
	 * @param key   缓存的键值
	 * @param value 缓存的值
	 */
	public <T> void setCacheObject(final String key, final T value) {
		redisTemplate.opsForValue().set(key, value);
	}

	/**
	 * 缓存基本的对象，Integer、String、实体类等
	 *
	 * @param key      缓存的键值
	 * @param value    缓存的值
	 * @param timeout  时间
	 * @param timeUnit 时间颗粒度
	 */
	public <T> void setCacheObject(final String key, final T value, final Long timeout, final TimeUnit timeUnit) {
		redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
	}

	/**
	 * 设置有效时间
	 *
	 * @param key     Redis键
	 * @param timeout 超时时间
	 * @return true=设置成功；false=设置失败
	 */
	public boolean expire(final String key, final long timeout) {
		return expire(key, timeout, TimeUnit.SECONDS);
	}

	/**
	 * 设置有效时间
	 *
	 * @param key     Redis键
	 * @param timeout 超时时间
	 * @param unit    时间单位
	 * @return true=设置成功；false=设置失败
	 */
	public boolean expire(final String key, final long timeout, final TimeUnit unit) {
		return redisTemplate.expire(key, timeout, unit);
	}

	/**
	 * 获得缓存的基本对象。
	 *
	 * @param key 缓存键值
	 * @return 缓存键值对应的数据
	 */
	public <T> T getCacheObject(final String key) {
		ValueOperations<String, T> operation = redisTemplate.opsForValue();
		return operation.get(key);
	}

	/**
	 * 删除单个对象
	 *
	 * @param key
	 */
	public boolean deleteObject(final String key) {
		return redisTemplate.delete(key);
	}

	/**
	 * 删除集合对象
	 *
	 * @param collection 多个对象
	 * @return
	 */
	public long deleteObject(final Collection collection) {
		return redisTemplate.delete(collection);
	}

	/**
	 * 缓存List数据
	 *
	 * @param key      缓存的键值
	 * @param dataList 待缓存的List数据
	 * @return 缓存的对象
	 */
	public <T> long setCacheList(final String key, final List<T> dataList) {
		Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
		return count == null ? 0 : count;
	}

	/**
	 * 获得缓存的list对象
	 *
	 * @param key 缓存的键值
	 * @return 缓存键值对应的数据
	 */
	public <T> List<T> getCacheList(final String key) {
		return redisTemplate.opsForList().range(key, 0, -1);
	}

	/**
	 * 缓存Set
	 *
	 * @param key     缓存键值
	 * @param dataSet 缓存的数据
	 * @return 缓存数据的对象
	 */
	public <T> long setCacheSet(final String key, final Set<T> dataSet) {
		Long count = redisTemplate.opsForSet().add(key, dataSet);
		return count == null ? 0 : count;
	}

	/**
	 * 获得缓存的set
	 *
	 * @param key
	 * @return
	 */
	public <T> Set<T> getCacheSet(final String key) {
		return redisTemplate.opsForSet().members(key);
	}

	/**
	 * 缓存Map
	 *
	 * @param key
	 * @param dataMap
	 */
	public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
		if (dataMap != null) {
			redisTemplate.opsForHash().putAll(key, dataMap);
		}
	}

	/**
	 * 获得缓存的Map
	 *
	 * @param key
	 * @return
	 */
	public <T> Map<String, T> getCacheMap(final String key) {
		return redisTemplate.opsForHash().entries(key);
	}

	/**
	 * 往Hash中存入数据
	 *
	 * @param key   Redis键
	 * @param hKey  Hash键
	 * @param value 值
	 */
	public <T> void setCacheMapValue(final String key, final String hKey, final T value) {
		redisTemplate.opsForHash().put(key, hKey, value);
	}

	/**
	 * 获取Hash中的数据
	 *
	 * @param key  Redis键
	 * @param hKey Hash键
	 * @return Hash中的对象
	 */
	public <T> T getCacheMapValue(final String key, final String hKey) {
		HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
		return opsForHash.get(key, hKey);
	}

	/**
	 * 获取多个Hash中的数据
	 *
	 * @param key   Redis键
	 * @param hKeys Hash键集合
	 * @return Hash对象集合
	 */
	public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hKeys) {
		return redisTemplate.opsForHash().multiGet(key, hKeys);
	}

	/**
	 * 获得缓存的基本对象列表
	 * 
	 * @param pattern 字符串前缀
	 * @return 对象列表
	 */
	public Collection<String> keys(final String pattern) {
		return redisTemplate.keys(pattern);
	}

	/**
	 * 获取分布式锁
	 * 
	 * @param lockKey     锁
	 * @param requestId   请求标识(唯一,用于自己的锁自己解)
	 * @param expireTime  过期时间(单位秒,建议大于业务处理时间)
	 * @param waitTimeout 等待时间(单位毫秒,建议大于过期时间)
	 * @return 是否获取成功
	 */
	public boolean tryLock(String lockKey, String requestId, int expireTime, long waitTimeout) {
		long nanoTime = System.nanoTime(); // 当前时间
		try {
			String script = "if redis.call('setNx',KEYS[1],ARGV[1]) then if redis.call('get',KEYS[1])==ARGV[1] then return redis.call('expire',KEYS[1],ARGV[2]) else return 0 end end";

			log.info("开始获取分布式锁-key[{}]", lockKey);
			int count = 0;
			do {
				RedisScript<Long> redisScript = new DefaultRedisScript<>(script, Long.class);

				log.info("尝试获取分布式锁-key[{}],requestId[{}],count[{}]", lockKey, requestId, count);
				Object result = redisTemplate.execute(redisScript, Collections.singletonList(lockKey), requestId,
						expireTime);
				if (SUCCESS.equals(result)) {
					log.info("尝试获取分布式锁-key[{}]成功", lockKey);
					return true;
				}

				Thread.sleep(500L);// 休眠500毫秒
				count++;
			} while ((System.nanoTime() - nanoTime) < TimeUnit.MILLISECONDS.toNanos(waitTimeout));

		} catch (Exception e) {
			log.error("尝试获取分布式锁-key[{}]异常", lockKey);
			log.error(e.getMessage(), e);
		}
		return false;
	}

	/**
	 * 释放锁
	 * 
	 * @param lockKey   锁
	 * @param requestId 请求标识
	 * @return 是否释放成功
	 */
	public boolean releaseLock(String lockKey, String requestId) {
		String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
		RedisScript<Long> redisScript = new DefaultRedisScript<Long>(script, Long.class);
		Object result = redisTemplate.execute(redisScript, Collections.singletonList(lockKey), requestId);
		if (SUCCESS.equals(result)) {
			return true;
		}
		return false;
	}

	/**
	 * @Title: getOrderId
	 * @Description: 生成19位订单号
	 * @param key
	 * @param delta
	 * @return
	 */
	public String getOrderId(String key, Long... delta) {
		try {
			Long dd = null;
			if (delta.length == 0) {
				dd = 1L;
			} else {
				dd = delta[0];
			}
			// 生成14位的时间戳(每秒使用新的时间戳当key)
			String timeStamp = new SimpleDateFormat("yyMMddHHmmss").format(new Date());
			// 获得redis-key
			String newKey = key + ":" + timeStamp;
			// 获取自增值（时间戳+自定义key）
			Long increment = incr(newKey, dd);
			// 设置时间戳生成的key的有效期为2秒
			expire(newKey, 2);

			String orderId = timeStamp + String.format("%07d", increment);
			log.info("Redis生成订单号:{}", orderId);
			return orderId;
		} catch (Exception e) {
			log.error("【异常】Redis生成订单号");
		}
		return null;
	}

	/**
	 * 递增
	 * 
	 * @param key 键
	 * @param by  要增加几(可以为负数，为负数时表示减)
	 * @return
	 */
	public long incr(String key, long delta) {
		return redisTemplate.opsForValue().increment(key, delta);
	}
}
