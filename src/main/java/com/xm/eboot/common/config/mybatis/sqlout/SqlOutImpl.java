package com.xm.eboot.common.config.mybatis.sqlout;

import org.apache.ibatis.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @ClassName: SqlOutImpl
 * @Description: 自定义mybatis打印sql实现类 <br />
 *               将debug日志输出成info日志 对sql进行
 * @author mengxc
 * @date 2020年10月22日 上午10:02:45
 */
public class SqlOutImpl implements Log {

	Logger log = LoggerFactory.getLogger(SqlOutImpl.class);

	public SqlOutImpl(String clazz) {
		// Do Nothing
	}

	@Override
	public boolean isDebugEnabled() {
		// return log.isDebugEnabled();
		// 将debug级别输出权限改成info级别
		return log.isInfoEnabled();

	}

	@Override
	public void debug(String s) {
		// log.debug(s);
		// debug日志输出成info级别日志
		log.info(s);
	}

	@Override
	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}

	@Override
	public void error(String s, Throwable e) {
		log.error(s, e);
	}

	@Override
	public void error(String s) {
		log.error(s);
	}

	@Override
	public void trace(String s) {
		log.trace(s);
	}

	@Override
	public void warn(String s) {
		log.warn(s);
	}
}